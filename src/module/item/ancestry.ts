import { ABCItemPF2e } from './abc';
import { AncestryData } from './data-definitions';

export class AncestryPF2e extends ABCItemPF2e {
    data!: AncestryData;
    _data!: AncestryData;
}
